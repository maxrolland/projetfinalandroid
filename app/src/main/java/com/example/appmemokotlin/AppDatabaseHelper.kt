package com.example.appmemokotlin

import android.content.Context
import androidx.room.Room
import com.example.appmemokotlin.database.AppDatabase

class AppDatabaseHelper(context: Context?) {

    companion object {
        private var dataBaseHelper: AppDatabaseHelper? = null
        private lateinit var database: AppDatabase

        @Synchronized
        fun getDatabase(context: Context): AppDatabase {
            if (dataBaseHelper == null)
                dataBaseHelper =
                    AppDatabaseHelper(
                        context.applicationContext
                    )

            return database
        }
    }

    init {
        database = context?.let {
            Room
                .databaseBuilder(it, AppDatabase::class.java, "memos.db")
                .allowMainThreadQueries()
                .build()
        }!!
    }
}