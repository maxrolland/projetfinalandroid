package com.example.appmemokotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.appmemokotlin.fragment.DetailFragment

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val intituleMemo = intent.getStringExtra("detail")

        // fragment :
        val fragment = DetailFragment()
        val bundle = Bundle()
        bundle.putString(DetailFragment.EXTRA_PARAM, intituleMemo)
        fragment.arguments = bundle

        // fragment manager :
        val fragmentManager = supportFragmentManager
        val fragmentTransaction =
            fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.conteneur_detail, fragment, "exemple2")
        fragmentTransaction.commit()
    }
}
