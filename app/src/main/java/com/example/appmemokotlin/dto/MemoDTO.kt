package com.example.appmemokotlin.dto

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "memos")
class MemoDTO(
    @PrimaryKey(autoGenerate = true)
    var memosId: Long = 0L,
    var intitule: String? = null){

    constructor(intitule: String) : this() {
        this.intitule = intitule
    }
}