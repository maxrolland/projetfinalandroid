package com.example.appmemokotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appmemokotlin.adapter.MemoAdapter
import com.example.appmemokotlin.dto.MemoDTO
import com.example.appmemokotlin.helper.ItemTouchHelperCallback
import com.example.appmemokotlin.repository.MainRepository
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    // MVVM
    private lateinit var mainViewModel: MainViewModel
    private lateinit var memoAdapter: MemoAdapter

    private var listMemos: MutableList<MemoDTO>? = ArrayList()
    private lateinit var itemTouchHelper: ItemTouchHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        mainViewModel.init(MainRepository())
        listMemos = mainViewModel.getLiveDataMemo()?.toMutableList()
        memoAdapter = MemoAdapter(listMemos!!, this)

        recycler_view_memo.setHasFixedSize(true)
        recycler_view_memo.layoutManager = LinearLayoutManager(this)
        itemTouchHelper = ItemTouchHelper(ItemTouchHelperCallback(memoAdapter))
        itemTouchHelper.attachToRecyclerView(recycler_view_memo)
        recycler_view_memo.adapter = memoAdapter
    }

    fun insererMemo(view: View) {
        if (ajout_memo.text.toString().isNotEmpty()) {
            val memo = MemoDTO(ajout_memo!!.text.toString())
            listMemos?.add(memo)
            mainViewModel.insererMemo(memo)
            recycler_view_memo.adapter?.notifyDataSetChanged()
        }
        // reset du champ d'ajout
        ajout_memo.text.clear()
    }
}
