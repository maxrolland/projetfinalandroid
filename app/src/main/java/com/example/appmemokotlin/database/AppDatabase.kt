package com.example.appmemokotlin.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.appmemokotlin.dao.MemosDAO
import com.example.appmemokotlin.dto.MemoDTO

@Database(entities = [MemoDTO::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun memosDAO(): MemosDAO
}