package com.example.appmemokotlin.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.appmemokotlin.R
import kotlinx.android.synthetic.main.fragment_detail.*

class DetailFragment : Fragment(),
    View.OnClickListener {

    companion object {
        const val EXTRA_PARAM = "detail"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (arguments != null && view != null) {
            val argument =
                requireArguments().getString(EXTRA_PARAM)
            text_memo_detail.text = argument
        }
    }

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }


}