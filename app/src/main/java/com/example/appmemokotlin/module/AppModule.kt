package com.example.appmemokotlin.module

import android.app.Application
import android.content.Context
import com.example.appmemokotlin.AppDatabaseHelper
import com.example.appmemokotlin.dao.MemosDAO
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    fun provideContext(application: Application): Context {
        return application
    }

    @Singleton
    @Provides
    fun provideMemoDAO(applicationContext: Context): MemosDAO {
        return AppDatabaseHelper.getDatabase(applicationContext).memosDAO()
    }
}