package com.example.appmemokotlin.repository

import com.example.appmemokotlin.dao.MemosDAO
import com.example.appmemokotlin.dto.MemoDTO
import com.example.appmemokotlin.di.DIApplication
import javax.inject.Inject


class MainRepository {

    @Inject lateinit var memoDAO: MemosDAO

    fun getLiveDataMemo(): List<MemoDTO> {
        return memoDAO.getListeMemos();
    }

    fun insertMemo(memo: MemoDTO) {
        memoDAO.insert(memo)
    }

    fun deleteMemo(memo: MemoDTO) {
        memoDAO.delete(memo)
    }

    init {
        DIApplication.getAppComponent()?.inject(this)
    }

}