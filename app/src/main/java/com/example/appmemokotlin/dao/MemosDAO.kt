package com.example.appmemokotlin.dao

import androidx.room.*
import com.example.appmemokotlin.dto.MemoDTO


@Dao
abstract class MemosDAO {

    @Query("SELECT * FROM memos")
    abstract fun getListeMemos(): MutableList<MemoDTO>

    @Insert
    abstract fun insert(vararg courses: MemoDTO?)

    @Update
    abstract fun update(vararg courses: MemoDTO?)

    @Delete
    abstract fun delete(vararg courses: MemoDTO?)
}