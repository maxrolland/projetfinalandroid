package com.example.appmemokotlin

import androidx.lifecycle.ViewModel
import com.example.appmemokotlin.dto.MemoDTO
import com.example.appmemokotlin.repository.MainRepository

class MainViewModel : ViewModel() {

    private lateinit var mainRepository: MainRepository

    // Initialisation :
    fun init(mainRepository: MainRepository) {
        this.mainRepository = mainRepository
    }

    fun getLiveDataMemo(): List<MemoDTO>? {
        return mainRepository.getLiveDataMemo()
    }

    fun insererMemo(memo: MemoDTO) {
        mainRepository.insertMemo(memo)
    }
}