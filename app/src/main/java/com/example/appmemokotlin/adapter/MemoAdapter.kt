package com.example.appmemokotlin.adapter

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.appmemokotlin.AppDatabaseHelper
import com.example.appmemokotlin.dto.MemoDTO
import com.example.appmemokotlin.DetailActivity
import com.example.appmemokotlin.fragment.DetailFragment
import com.example.appmemokotlin.R
import kotlinx.android.synthetic.main.memo_list_item.view.*
import java.util.*

class MemoAdapter(listeNotes: MutableList<MemoDTO>, main: AppCompatActivity) : RecyclerView.Adapter<MemoAdapter.MemoViewHolder?>() {

    private var listeMemos: MutableList<MemoDTO> = ArrayList()
    private val main: AppCompatActivity

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemoViewHolder {
        val viewMemo: View =
            LayoutInflater.from(parent.context).inflate(R.layout.memo_list_item, parent, false)
        return MemoViewHolder(viewMemo)
    }

    override fun onBindViewHolder(holder: MemoViewHolder, position: Int) {
        holder.bind(listeMemos[position])
    }

    override fun getItemCount(): Int {
        return listeMemos.size
    }

    fun onItemDismiss(view: RecyclerView.ViewHolder) {
        if (view.adapterPosition > -1) {
            AppDatabaseHelper.getDatabase(view.itemView.context).memosDAO()?.delete(listeMemos[view.adapterPosition])
            listeMemos.removeAt(view.adapterPosition)
            notifyItemRemoved(view.adapterPosition)
        }
    }

    inner class MemoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // private val TAG = "Im the tag guy: "

        init {
            itemView.setOnClickListener { view ->
                val memo: MemoDTO = listeMemos[adapterPosition]

                // gestion des sharedPreferences
                val preferences: SharedPreferences =
                    androidx.preference.PreferenceManager.getDefaultSharedPreferences(view.context)
                val editor = preferences.edit()
                editor.putInt("last", adapterPosition)
                editor.apply()

                if (main.findViewById<View?>(R.id.conteneur_detail) == null) {
                    val intent = Intent(view.context, DetailActivity::class.java)
                    intent.putExtra("detail", memo.intitule)
                    view.context.startActivity(intent)
                } else {
                    // fragment :
                    val fragment = DetailFragment()
                    val bundle = Bundle()
                    bundle.putString(DetailFragment.EXTRA_PARAM, memo.intitule)
                    fragment.arguments = bundle

                    // fragment manager :
                    val fragmentManager =
                        main.supportFragmentManager
                    // transaction :
                    val fragmentTransaction =
                        fragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.conteneur_detail, fragment, "exemple2")
                    fragmentTransaction.commit()
                }
            }
        }

        fun bind(memo:MemoDTO) = with(itemView){
            libelle_item_memo.text = memo.intitule
        }
    }

    init {
        this.listeMemos = listeNotes
        this.main = main
    }
}