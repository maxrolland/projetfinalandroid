package com.example.appmemokotlin.helper

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.appmemokotlin.adapter.MemoAdapter
import androidx.recyclerview.widget.ItemTouchHelper.RIGHT

class ItemTouchHelperCallback(adapter: MemoAdapter) :
    ItemTouchHelper.Callback() {
    private val myAdapter: MemoAdapter = adapter
    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: ViewHolder): Int {
        //La suppression apparait quand on swipe sur la DROITE uniquement
        val dragFlagsRight = RIGHT
        return makeMovementFlags(
            dragFlagsRight,
            dragFlagsRight
        )
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: ViewHolder,
        target: ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: ViewHolder, direction: Int) {
        myAdapter.onItemDismiss(viewHolder)
        myAdapter.notifyDataSetChanged()
    }
}